using Microsoft.AspNetCore.Mvc.RazorPages;
using SaliensSite;

namespace Saliens.Pages
{
    public class PlanetModel : PageModel
    {
        public SaliensService Saliens { get; }

        public PlanetModel(SaliensService saliens)
        {
            Saliens = saliens;
        }

        public void OnGet()
        {
        }
    }
}
