using Microsoft.AspNetCore.Mvc.RazorPages;
using SaliensSite;

namespace Saliens.Pages
{
    public class IndexModel : PageModel
    {
        public SaliensService Saliens { get; }

        public IndexModel(SaliensService saliens)
        {
            Saliens = saliens;
        }

        public void OnGet()
        {

        }
    }
}
