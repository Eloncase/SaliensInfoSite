using Microsoft.AspNetCore.Mvc.RazorPages;
using SaliensSite;

namespace Saliens.Pages
{
    public class ClansModel : PageModel
    {
        public SaliensService Saliens { get; }

        public ClansModel(SaliensService saliens)
        {
            Saliens = saliens;
        }

        public void OnGet()
        {
        }
    }
}
