﻿using System.Text.Json;

namespace SaliensSite
{
    public class SaliensService
    {
        private readonly static string DumpPath = Path.Combine(Directory.GetCurrentDirectory(), "SaliensInfoDump", "SaliensDump");
        private readonly static string SteamPath = Path.Combine(DumpPath, "AppInfo.json");
        public readonly Dictionary<int, AppInfo> SteamAppInfo;
        public readonly Dictionary<int, Planet> PlanetDict;
        public IEnumerable<Planet> PlanetList => PlanetDict.Values;

        private readonly ILogger<SaliensService> logger;

        public SaliensService(ILogger<SaliensService> logger)
        {
            this.logger = logger;

            var getPlanetsPath = Path.Combine(DumpPath, "GetPlanets.json");
            if (!Directory.Exists(DumpPath)) throw new Exception("There is no info dump to create site from.");
            if (!File.Exists(getPlanetsPath)) throw new Exception("GetPlanets.json was not found inside dump folder.");

            var list = JsonSerializer.Deserialize<JsonResponse<GetPlanetsResponse>>(File.ReadAllText(getPlanetsPath)).Response.Planets;
            PlanetDict = JsonSerializer.Deserialize<JsonResponse<GetPlanetsResponse>>(File.ReadAllText(getPlanetsPath)).Response.Planets
                .ToDictionary(p => int.Parse(p.Id), p =>
                {
                    p.Zones = GetPlanet(p.Id).Zones;
                    return p;
                });

            if (File.Exists(SteamPath))
            {
                SteamAppInfo = JsonSerializer.Deserialize<Dictionary<int, AppInfo>>(File.ReadAllText(SteamPath));
            }
            else
            {
                SteamAppInfo = [];
            }
        }

        public async Task<List<AppInfo>> GetGames(IEnumerable<int> appIds)
        {
            var http = new HttpClient();
            http.DefaultRequestHeaders.TryAddWithoutValidation("Accept-Language", "en-US,en;");
            http.DefaultRequestHeaders.TryAddWithoutValidation("Cookie", "steamCountry=US;");
            var games = new List<AppInfo>();
            var throttleAfter = 5;
            var index = 0;

            logger.LogInformation($"Getting AppInfo for {appIds.Count()} apps");

            foreach (var app in appIds)
            {
                logger.LogInformation($"Getting AppInfo for {app}");
                if (SteamAppInfo.TryGetValue(app, out AppInfo value))
                {
                    games.Add(value);
                }
                else
                {
                    if (index == throttleAfter)
                    {
                        index = 0;
                        logger.LogInformation("Throttling");
                        await Task.Delay(1000);
                    }

                    try
                    {
                        var data = await http.GetStringAsync(new Uri($"https://store.steampowered.com/api/appdetails/?appids={app}&filters=basic,price_overview&cc=us"));
                        var game = JsonSerializer.Deserialize<Dictionary<int, AppInfoResponse>>(data).Values.First();

                        if (!game.Success) throw new Exception($"Couldn't get app info for {app}");

                        games.Add(game.Data);
                        SteamAppInfo.Add(app, game.Data);
                    }
                    catch (Exception e)
                    {
                        logger.LogError(e, "Error while getting appinfo");
                    }

                    index++;
                }
            }

            SaveSteamInfo();
            return games;
        }

        private void SaveSteamInfo()
        {
            File.WriteAllText(SteamPath, JsonSerializer.Serialize(SteamAppInfo));
        }

        private static Planet GetPlanet(string id)
        {
            var planetPath = Path.Combine(DumpPath, "GetPlanet", $"{id}.json");
            return JsonSerializer.Deserialize<JsonResponse<GetPlanetsResponse>>(File.ReadAllText(planetPath)).Response.Planets.First();
        }
    }
}
