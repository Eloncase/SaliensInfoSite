using System.Text.Json.Serialization;

namespace SaliensSite
{
    public class JsonResponse<T>
    {
        [JsonPropertyName("response")]
        public T Response { get; set; }
    }

    public class GetPlanetsResponse
    {
        [JsonPropertyName("planets")]
        public List<Planet> Planets { get; set; }
    }

    public class Planet
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }
        [JsonPropertyName("state")]
        public PlanetState State { get; set; }
        [JsonPropertyName("giveaway_apps")]
        public List<int> Apps { get; set; }
        [JsonPropertyName("top_clans")]
        public List<Clan> Clans { get; set; }
        [JsonPropertyName("zones")]
        public List<Zone> Zones { get; set; }
    }

    public class PlanetState
    {
        [JsonPropertyName("name")]
        public string Name { get; set; }
        [JsonPropertyName("image_filename")]
        public string ImageFilename { get; set; }
        [JsonPropertyName("difficulty")]
        public int Difficulty { get; set; }
        [JsonPropertyName("giveaway_id")]
        public string GiveawayId { get; set; }
        [JsonPropertyName("active")]
        public bool Active { get; set; }
        [JsonPropertyName("activation_time")]
        public long ActivationTime { get; set; }
        [JsonPropertyName("captured")]
        public bool Captured { get; set; }
        [JsonPropertyName("capture_time")]
        public long CaptureTime { get; set; }
        [JsonPropertyName("capture_progress")]
        public double CaptureProgress { get; set; }
        [JsonPropertyName("total_joins")]
        public int TotalJoins { get; set; }
        [JsonPropertyName("current_players")]
        public int CurrentPlayers { get; set; }
    }

    public class Clan
    {
        [JsonPropertyName("num_zones_controled")]
        public int ZonesControlled { get; set; }
        [JsonPropertyName("clan_info")]
        public ClanInfo Info { get; set; }
    }
    public class ClanInfo
    {
        [JsonPropertyName("accountid")]
        public int AccountId { get; set; }
        [JsonPropertyName("name")]
        public string Name { get; set; }
        [JsonPropertyName("avatar")]
        public string Avatar { get; set; }
        [JsonPropertyName("url")]
        public string Url { get; set; }
    }

    public class Zone
    {
        [JsonPropertyName("zone_position")]
        public int Position { get; set; }
        [JsonPropertyName("leader")]
        public ClanInfo Leader { get; set; }
        [JsonPropertyName("type")]
        public int Type { get; set; }
        [JsonPropertyName("difficulty")]
        public int Difficulty { get; set; }
        [JsonPropertyName("captured")]
        public bool Captured { get; set; }
        [JsonPropertyName("capture_progress")]
        public double CaptureProgress { get; set; }
        [JsonPropertyName("top_clans")]
        public List<ClanInfo> Clans { get; set; }
    }

    public class AppInfoResponse
    {
        [JsonPropertyName("success")]
        public bool Success { get; set; }

        [JsonPropertyName("data")]
        public AppInfo Data { get; set; }
    }

    public class AppInfo
    {
        [JsonPropertyName("type")]
        public string Type { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("steam_appid")]
        public long SteamAppid { get; set; }

        //[JsonPropertyName("required_age")]
        //public string RequiredAge { get; set; }

        [JsonPropertyName("is_free")]
        public bool IsFree { get; set; }

        [JsonPropertyName("dlc")]
        public List<long> Dlc { get; set; }

        [JsonPropertyName("detailed_description")]
        public string DetailedDescription { get; set; }

        [JsonPropertyName("about_the_game")]
        public string AboutTheGame { get; set; }

        [JsonPropertyName("short_description")]
        public string ShortDescription { get; set; }

        [JsonPropertyName("supported_languages")]
        public string SupportedLanguages { get; set; }

        [JsonPropertyName("reviews")]
        public string Reviews { get; set; }

        [JsonPropertyName("header_image")]
        public string HeaderImage { get; set; }

        [JsonPropertyName("website")]
        public string Website { get; set; }

        [JsonPropertyName("price_overview")]
        public AppPrice PriceOverview { get; set; }
    }

    public class AppPrice
    {
        [JsonPropertyName("currency")]
        public string Currency { get; set; }

        [JsonPropertyName("initial")]
        public long Initial { get; set; }

        [JsonPropertyName("final")]
        public long Final { get; set; }

        [JsonPropertyName("discount_percent")]
        public long DiscountPercent { get; set; }

        [JsonPropertyName("initial_formatted")]
        public string InitialFormatted { get; set; }

        [JsonPropertyName("final_formatted")]
        public string FinalFormatted { get; set; }
    }
}