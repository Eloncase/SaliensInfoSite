using Microsoft.Extensions.FileProviders;
using SaliensSite;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddRazorPages();
builder.Services.AddSingleton<SaliensService>();

var app = builder.Build();

app.Services.GetRequiredService<SaliensService>();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
}
app.UseStaticFiles();

app.UseStaticFiles(new StaticFileOptions()
{
    FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "SaliensInfoDump", "SaliensDump", "Images")),
    RequestPath = new PathString("/images")
});

app.UseRouting();

app.UseAuthorization();

app.MapRazorPages();

app.Run();
